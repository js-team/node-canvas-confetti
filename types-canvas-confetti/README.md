# Installation
> `npm install --save @types/canvas-confetti`

# Summary
This package contains type definitions for canvas-confetti (https://github.com/catdad/canvas-confetti#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/canvas-confetti.

### Additional Details
 * Last updated: Wed, 13 Jul 2022 03:32:15 GMT
 * Dependencies: none
 * Global values: `confetti`

# Credits
These definitions were written by [Martin Tracey](https://github.com/matracey), and [Josh Batley](https://github.com/joshbatley).
